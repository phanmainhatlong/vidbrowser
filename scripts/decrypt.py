from cryptography.fernet import Fernet
from pathlib import Path
import os


def call(password: str = None):
    if os.path.exists(rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag"):
        if not password:
            with open(f"{os.environ['USERPROFILE']}\\AppData\\Local\\MeinVidBrowser\\key", "rb") as file:
                key = file.read()

            f = Fernet(key)

            with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "rb+") as file:
                data = file.read()
                decrypted = f.decrypt(data)
                file.truncate(0)

            with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "wb") as file:
                file.write(decrypted)

        os.remove(
            rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag")
