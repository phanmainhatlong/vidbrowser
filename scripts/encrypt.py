from cryptography.fernet import Fernet
from pathlib import Path
import os


def call():
    if not os.path.exists(rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag"):
        key = Fernet.generate_key()

        f = Fernet(key=key)
        encrypted = b""

        with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "rb") as file:
            data = file.read()
            encrypted = f.encrypt(data)

        with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "w") as file:
            file.truncate(0)
            file.write(str(encrypted)[2:-1])

        if not os.path.exists(f"{os.environ['USERPROFILE']}\\AppData\\Local\\MeinVidBrowser"):
            os.mkdir(
                f"{os.environ['USERPROFILE']}\\AppData\\Local\\MeinVidBrowser")

        with open(f"{os.environ['USERPROFILE']}\\AppData\\Local\\MeinVidBrowser\\key", "wb") as file:
            file.write(key)

        #os.system(
        #    f"attrib +r +h \"{os.environ['USERPROFILE']}\\AppData\\Local\\MeinVidBrowser\" /s /d")

        with open(rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag", "w"):
            pass
