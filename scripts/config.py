import tkinter as tk
import json
import os
from scripts.constants import *
from tkinter import messagebox as mb
from pathlib import Path


class ConfigSettings:
    def __init__(self, master) -> None:
        self.root = tk.Toplevel(master)
        self.root.title("Vidbrowser: Settings")
        self.root.resizable(width=False, height=False)
        self.root.wm_transient(master)
        self.root.geometry(
            f"{CONF_WIDTH}x{CONF_HEIGHT}+{int(self.root.winfo_screenwidth()/2 - CONF_WIDTH/2)}+{int(self.root.winfo_screenheight()/2 - CONF_HEIGHT/2)}")

        self.do_walk = False
        self.do_walk_display_str = tk.StringVar(self.root, f"{self.do_walk}")
        self.update_str = tk.StringVar(
            self.root, r"path to search folder or key to remove")
        self.paths = {}

        self.test_json()
        self.widget_init()

    def test_json(self):
        if os.path.exists(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json"):
            return
        try:
            with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "r") as file:
                json.load(file)
        except:
            mb.showerror(
                "Error", f"Error opening {Path(__file__).resolve().parent.parent}\settings\settings.json")
        else:
            self.get_data()

    def get_data(self):
        with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "r") as file:
            data = json.load(file)
            try:
                self.do_walk = data['do_walk']
                self.do_walk_display_str.set(f"{self.do_walk}")
            except:
                self.do_walk = False

            try:
                for _, e in enumerate(data['paths']):
                    self.paths[e] = data['paths'][e]
            except:
                pass

    def widget_init(self):
        tk.Label(self.root, text="Do_walk: ", font=FONT).place(
            x=20, y=10, width=100, height=30)

        self.do_walk_display = tk.Label(
            self.root, textvariable=self.do_walk_display_str, font=FONT,
            fg="#47b526" if self.do_walk == True else "#c21010"
        )

        self.dowalk_toggle = tk.Button(
            self.root, text="Toggle", font=FONT,
            command=self.toggle
        )

        self.update_entry = tk.Entry(
            self.root, textvariable=self.update_str, font=FONT_I
        )

        self.append_button = tk.Button(
            self.root, text="Append", font=FONT,
            command=self.append_data
        )

        self.remove_button = tk.Button(
            self.root, text="Remove", font=FONT,
            command=self.remove_data
        )

        self.update_button = tk.Button(
            self.root, text="Update", font=FONT,
            command=self.update_data
        )

        self.editor = tk.Text(self.root, wrap="none", fg="#000", bg="#fff")
        self.editorxscroll = tk.Scrollbar(
            self.root, command=self.editor.xview, orient="horizontal")
        self.editoryscroll = tk.Scrollbar(self.root, command=self.editor.yview)

        for i in self.paths:
            self.editor.insert(tk.END, rf"{i}: {self.paths[i]}" + "\n")
        self.editor.config(xscrollcommand=self.editorxscroll.set,
                           yscrollcommand=self.editoryscroll.set)
        self.editor.config(state=tk.DISABLED)

        self.widget_draw()

    def toggle(self):
        self.do_walk = True if self.do_walk == False else False
        self.do_walk_display_str.set(f"{self.do_walk}")
        self.do_walk_display.config(
            fg="#47b526" if self.do_walk == True else "#c21010")

    def widget_draw(self):
        self.do_walk_display.place(x=170, y=10, width=50, height=30)
        self.dowalk_toggle.place(x=250, y=10, width=80, height=30)
        self.editor.place(x=20, y=60, width=660, height=400)
        self.editorxscroll.place(x=350, y=480, width=660, anchor='s')
        self.editoryscroll.place(relx=1, y=60, height=400, anchor='ne')
        self.update_entry.place(x=20, y=500, width=350, height=30)
        self.append_button.place(x=370, y=500, width=90, height=30)
        self.remove_button.place(x=480, y=500, width=90, height=30)
        self.update_button.place(x=300, y=550, width=100, height=30)

    def append_data(self):
        self.editor.config(bg=GRAY)
        len_of_paths = len(self.paths)
        self.paths[f"folder{len_of_paths}"] = rf"{self.update_str.get()}"
        self.editor.config(state=tk.NORMAL)
        self.editor.delete("1.0", tk.END)
        for i in self.paths:
            self.editor.insert(tk.END, rf"{i}: {self.paths[i]}" + "\n")
        self.editor.config(bg="#fff", state=tk.DISABLED)

    def remove_data(self):
        tmp_dict = {}
        self.editor.config(bg=GRAY)
        self.paths.pop(self.update_str.get(), None)
        try:
            self.paths = {k: v for k, v in self.paths.items() if Path(
                rf"{v}") != Path(rf"{self.update_str.get()}")}
        except:
            pass
        self.editor.config(state=tk.NORMAL)
        self.editor.delete("1.0", tk.END)
        for i, e in enumerate(self.paths):
            tmp_dict[f"folder{i}"] = self.paths[e]
        self.paths = {k: v for k, v in tmp_dict.items()}
        for i in self.paths:
            self.editor.insert(tk.END, rf"{i}: {self.paths[i]}" + "\n")
        self.editor.config(bg="#fff", state=tk.DISABLED)

    def update_data(self):
        if os.path.exists(rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag"):
            encrypted_prompt = mb.askyesno("Prompt", "Settings file is encrypted. Are you sure that you want to continue?")
            if not encrypted_prompt:
                return

        with open(rf"{Path(__file__).resolve().parent.parent}\settings\settings.json", "r+") as file:
            if encrypted_prompt:
                data = {}
                file.truncate(0)
                data['do_walk'] = self.do_walk
                data['paths'] = {}
                for _, e in enumerate(self.paths):
                    data['paths'][e] = self.paths[e]
                json.dump(data, file, indent=4)
                file.truncate()
                os.remove(rf"{Path(__file__).resolve().parent.parent}\settings\encrypted.flag")
                return
            try:
                data = json.load(file)
            except:
                mb.showerror(
                    "Error", "An error occurred when opening settings.json.")
                return
            else:
                data['do_walk'] = self.do_walk
                data['paths'] = {}
                for _, e in enumerate(self.paths):
                    data['paths'][e] = self.paths[e]
                file.seek(0)
                json.dump(data, file, indent=4)
                file.truncate()
