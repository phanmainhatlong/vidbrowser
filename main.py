import json
import os
import subprocess
import pymediainfo
import scripts.decrypt as dec
import scripts.encrypt as enc
import scripts.pathcheck as pc
import tkinter as tk
from tkinter import messagebox as mb
from pathlib import Path
from threading import Thread
from scripts.config import ConfigSettings
from scripts.constants import *


class GUI:
    def __init__(self, master) -> None:
        self.root = master
        self.root.title("Video Browser")
        self.root.resizable(width=False, height=False)
        self.root.geometry(
            f"{WIN_WIDTH}x{WIN_HEIGHT}+{int(self.root.winfo_screenwidth() / 2 - WIN_WIDTH / 2)}+{int(self.root.winfo_screenheight() / 2 - WIN_HEIGHT / 2)}")
        self.root.protocol("WM_DELETE_WINDOW", self.handle_window_close)

        self.search_paths = []
        self.files = []
        self.online_url = tk.StringVar(self.root, r"")
        self.input_path = tk.StringVar(self.root, r"")
        self.media_info = pymediainfo.MediaInfo
        self.do_walk = False
        self.do_window_close = False
        self.settings_opened = False
        self.encrypting = False
        self.decrypting = False
        self.refreshing = False

        self.get_walk()
        self.init_widget()
        self.draw()

    def handle_window_close(self):
        self.root.destroy()
        self.do_window_close = True

    def get_walk(self):
        if os.path.exists(rf"{Path(__file__).resolve().parent}\settings\settings.json"):
            return
        try:
            with open(rf"{Path(__file__).resolve().parent}\settings\settings.json", "r") as file:
                data = json.load(file)
        except:
            mb.showerror(
                "Error", f"Error opening {Path(__file__).resolve().parent.parent}\settings\settings.json")
        else:
            self.do_walk = data['do_walk']

    def get_search_path(self):
        if os.path.exists(rf"{Path(__file__).resolve().parent}\settings\settings.json"):
            return
        self.search_paths = []
        self.files = []
        try:
            with open(rf"{Path(__file__).resolve().parent}\settings\settings.json", "r") as file:
                data = json.load(file)
        except:
            mb.showerror(
                "Error", f"Error opening {Path(__file__).resolve().parent.parent}\settings\settings.json")
        else:
            if not self.do_walk:
                for i in data['paths']:
                    if self.do_window_close:
                        return
                    self.search_paths.append(data['paths'][i])
            elif self.do_walk:
                for i in data['paths']:
                    if self.do_window_close:
                        return
                    if not pc.is_pathname_valid(data['paths'][i]):
                        break
                    if not os.path.exists(data['paths'][i]):
                        break
                    for e in self.search_paths:
                        if Path(e) in Path(data['paths'][i]).parents:
                            break
                    else:
                        self.search_paths.append(data['paths'][i])
        finally:
            Thread(target=self.listbox_init).start()

    def get_file_from_input(self):
        self.search_paths = []
        self.search_paths.append(self.input_path.get())
        self.remove_listbox_items()
        self.listbox_init()

    def init_widget(self):
        self.vid_select = tk.Listbox(
            self.root, selectmode="single"
        )

        self.yscrollbar = tk.Scrollbar(
            self.root, command=self.vid_select.yview
        )

        self.xscrollbar = tk.Scrollbar(
            self.root, command=self.vid_select.xview, orient='horizontal'
        )

        self.online_video_entry = tk.Entry(
            self.root, textvariable=self.online_url, font=FONT
        )

        self.online_play_button = tk.Button(
            self.root, text="Play online video", font=FONT,
            command=lambda: Thread(target=self.play_online_video).start()
        )

        self.custom_path_entry = tk.Entry(
            self.root, textvariable=self.input_path, font=FONT
        )

        self.custom_path_search = tk.Button(
            self.root, text="Search", font=FONT,
            command=self.get_file_from_input
        )

        self.menubar = tk.Menu(self.root, tearoff=False)
        self.root.config(menu=self.menubar)

        self.file_menu = tk.Menu(self.menubar, tearoff=False)
        self.menubar.add_cascade(label="File", menu=self.file_menu)
        self.file_menu.add_command(
            label="Encrypt", command=lambda: Thread(target=self.callenc).start())
        self.file_menu.add_command(
            label="Decrypt", command=lambda: Thread(target=self.calldec).start())
        self.file_menu.add_command(
            label="Refresh", command=lambda: Thread(target=self.refresh).start())
        self.file_menu.add_command(label="Config", command=self.settings_edit)
        self.file_menu.add_command(
            label="Quit", command=self.handle_window_close)

    def draw(self):
        self.vid_select.place(
            x=0, y=0, width=583, height=490
        )

        self.yscrollbar.place(
            relx=1, y=0, height=490, anchor='ne'
        )

        self.xscrollbar.place(
            x=300, y=507, width=600, anchor='s'
        )

        self.online_video_entry.place(
            x=30, y=530, width=380, height=50
        )

        self.online_play_button.place(
            x=420, y=530, width=150, height=50
        )

        self.custom_path_entry.place(
            x=30, y=600, width=380, height=50
        )

        self.custom_path_search.place(
            x=420, y=600, width=150, height=50
        )

        self.vid_select.config(
            yscrollcommand=self.yscrollbar.set, xscrollcommand=self.xscrollbar.set)
        self.vid_select.bind(
            '<Double-Button>', lambda x: Thread(target=self.watch).start())

        Thread(target=self.get_search_path).start()

    def listbox_init(self):
        for path in self.search_paths:
            for walkpath, _, files in os.walk(path):
                for file in files:
                    if self.encrypting or self.refreshing or self.do_window_close or self.settings_opened:
                        return
                    parse = self.media_info.parse(rf"{walkpath}\{file}")
                    if (any(track.track_type == "Video" for track in parse.tracks)):
                        self.files.append(rf"{walkpath}\{file}")
                        try:
                            self.vid_select.insert(tk.END, file)
                        except:
                            return
                if not self.do_walk:
                    break

    def remove_listbox_items(self):
        self.vid_select.delete(0, tk.END)

    def watch(self, *args):
        try:
            subprocess.run([rf"{os.getcwd()}\mpv\mpv.exe",
                           rf"{self.files[self.vid_select.curselection()[0]]}"], check=True)
        except:
            mb.showerror("Error:", "Please select a video file")

    def play_online_video(self):
        mb.showinfo(
            "Info", rf"Stream {self.online_url.get()} already started. Don't press playbutton again")
        try:
            subprocess.run([rf"{os.getcwd()}\mpv\mpv.exe",
                           rf"{self.online_url.get()}"], check=True)
        except Exception as e:
            mb.showerror("Error:", rf"{e}")

    def callenc(self):
        self.menubar.entryconfig("File", state="disabled")
        self.encrypting = True
        self.vid_select.config(state=tk.DISABLED)
        self.custom_path_search.config(state=tk.DISABLED)
        enc.call()
        self.vid_select.config(state=tk.NORMAL)
        self.custom_path_search.config(state=tk.NORMAL)
        self.remove_listbox_items()
        self.encrypting = False
        self.menubar.entryconfig("File", state="normal")

    def calldec(self):
        self.menubar.entryconfig("File", state="disabled")
        self.vid_select.config(state=tk.DISABLED)
        self.custom_path_search.config(state=tk.DISABLED)
        dec.call()
        self.vid_select.config(state=tk.NORMAL)
        self.get_walk()
        Thread(target=self.get_search_path).start()
        self.custom_path_search.config(state=tk.NORMAL)
        self.menubar.entryconfig("File", state="normal")

    def refresh(self):
        self.menubar.entryconfig("File", state="disabled")
        self.refreshing = True
        self.remove_listbox_items()
        self.custom_path_search.config(state=tk.DISABLED)
        self.get_walk()
        self.refreshing = False
        Thread(target=self.get_search_path).start()
        self.custom_path_search.config(state=tk.NORMAL)
        self.menubar.entryconfig("File", state="normal")

    def settings_edit(self):
        self.settings_opened = True
        self.top = ConfigSettings(self.root)
        self.top.root.protocol("WM_DELETE_WINDOW", self.on_settings_close)

    def on_settings_close(self):
        self.settings_opened = False
        self.top.root.destroy()
        self.refresh()


if __name__ == "__main__":
    root = tk.Tk()
    gui = GUI(root)
    root.mainloop()
